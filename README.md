# signify-csig-tools

Sign and verify files as documented at https://github.com/drupal/php-signify in a somewhat platform independent manner

## Motivation

Being able to combine the simplicity of signify with the flexibility of expiring temporary keys is useful.
This project aims to provide some simple Shell scripts that run natively on different flavors of Linux and BSD.

## Batteries not included

Some platforms need additional packages in order for the scripts to work properly.
See the platform specific readme files for more details.
