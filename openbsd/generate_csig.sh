#!/bin/sh
set -e
intseckey=$1
introotsig=$2
filetohashandsign=$3

if [ -z $intseckey ] || [ -z $introotsig ] || [ -z $filetohashandsign ]; then
  >&2 echo "USAGE: $0 intermediate_secret_key.sec root_intermediate_signature.sig file_to_hash_and_sign  > final.csig"
  exit 1
fi

if [ ! -f "$intseckey" ]; then
   >&2 echo "FILE NOT FOUND: $intseckey"
   exit 1
fi

if [ ! -f "$introotsig" ]; then
   >&2 echo "FILE NOT FOUND: $introotsig"
   exit 1
fi

if [ ! -f "$filetohashandsign" ]; then
   >&2 echo "FILE NOT FOUND: $filetohashandsign"
   exit 1
fi

>&2 echo "Outputting csig for $filetohashandsign"
cat $introotsig

# OpenBSD has the sha512 command
hash=$(sha512 $filetohashandsign)

echo "$hash" | signify -S -e -s $intseckey -m - -x -

